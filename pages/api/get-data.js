import axios from "axios"
import {withSentry} from '@sentry/nextjs';
import * as Sentry from  '@sentry/nextjs';

const handler = async(req , res)=>{
    try {
        const resp = await axios.get('https://nextjs-vercel-deployment-using-jest.vercel.app/api/hello-world');
        console.log(resp.data);
        throw new Error("Error message")
        res.json(resp.data);
    } catch (er) {
        Sentry.captureException(er);
        console.log(er);
        res.json({});
    }
    
}
export default withSentry(handler);
